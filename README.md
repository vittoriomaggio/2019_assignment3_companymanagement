# 2019_assignment3_CompanyManagement
## How to execute
### Package the app
`./mvnw clean package spring-boot:repackage`

### Run the app
`java -Djava.security.egd=file:/dev/./urandom -jar target/companymanagement.jar`

### Connect to
`http://127.0.0.1:8080/`