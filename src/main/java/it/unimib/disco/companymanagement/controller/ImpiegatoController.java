package it.unimib.disco.companymanagement.controller;

import it.unimib.disco.companymanagement.exception.ImpiegatoNotFoundException;
import it.unimib.disco.companymanagement.model.Dipartimento;
import it.unimib.disco.companymanagement.model.Impiegato;
import it.unimib.disco.companymanagement.model.Progetto;
import it.unimib.disco.companymanagement.repository.DipartimentoRepository;
import it.unimib.disco.companymanagement.repository.ImpiegatoRepository;
import it.unimib.disco.companymanagement.repository.ProgettoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ImpiegatoController { 

	private final ImpiegatoRepository impiegatoRepository;
	private final DipartimentoRepository dipartimentoRepository;
	private final ProgettoRepository progettoRepository;

	@Autowired
	public ImpiegatoController(ImpiegatoRepository impiegatoRepository,DipartimentoRepository dipartimentoRepository, ProgettoRepository progettoRepository) {
	  this.impiegatoRepository = impiegatoRepository;
	  this.dipartimentoRepository = dipartimentoRepository;
	  this.progettoRepository = progettoRepository;
	}

	@GetMapping("/impiegati")  
	public ModelAndView impiegati(@RequestParam(required=false) String msg) {
		ModelAndView modelAndView = new ModelAndView();
		Iterable<Impiegato> impiegati = impiegatoRepository.findAll();
		modelAndView.addObject("impiegati", impiegati);
		modelAndView.addObject("messaggio", msg);
		modelAndView.setViewName("impiegati");

		return modelAndView;
	}

	@GetMapping("/impiegati/aggiungiImpiegato") 
	public ModelAndView aggiungiImpiegato(Impiegato impiegato, ModelAndView modelAndView, BindingResult bindingResult) {
		Iterable<Dipartimento> dipartimenti = dipartimentoRepository.findAll();
		Iterable<Impiegato> impiegati = impiegatoRepository.findAll();
		modelAndView.addObject("dipartimenti", dipartimenti);
		modelAndView.addObject("impiegati", impiegati);  

		modelAndView.addObject("impiegato", impiegato);
		modelAndView.setViewName("aggiungiImpiegato");
		return modelAndView;
	}

	@PostMapping("/salvaImpiegato")
	public ModelAndView salvaImpiegato(Impiegato impiegato, BindingResult userloginResult, ModelAndView modelAndView) throws ImpiegatoNotFoundException {
		//Se il campo "Subordinato a" è Null, allora l'Impiegato è un responsabile
		if (impiegato.getResponsabile() == null) { 
			impiegato.setResponsabile(impiegato);
		}
		impiegatoRepository.save(impiegato);
		impiegatoRepository.findById(impiegato.getId()).orElseThrow(() -> new ImpiegatoNotFoundException("Errore durante il salvataggio, riprovare"));
		
		//Aggiorno il numero di impiegati di un Dipartimento
		Dipartimento dipartimento = impiegato.getDipartimento();
		dipartimento.setNumeroImp(dipartimento.getNumeroImp() + 1);
		dipartimentoRepository.save(dipartimento);

		String messaggio = "Impiegato salvato con successo";
		return new ModelAndView("redirect:/impiegati?msg=" + messaggio);
	}

	@GetMapping("/impiegati/modificaImpiegato/{id}")
	public ModelAndView modificaImpiegato (@PathVariable("id") long id, ModelAndView modelAndView) throws ImpiegatoNotFoundException {

		Impiegato impiegato = impiegatoRepository.findById(id).orElseThrow(() -> new ImpiegatoNotFoundException("Id Impiegato non valido, id:" + id));
		Iterable<Dipartimento> dipartimenti = dipartimentoRepository.findAll();
		Iterable<Impiegato> impiegati = impiegatoRepository.findAll();
		modelAndView.addObject("impiegati", impiegati); // elenco per 'Subordinato a'
		modelAndView.addObject("impiegato", impiegato); // impiegato da modificare 
		modelAndView.addObject("dipartimenti", dipartimenti);
		modelAndView.setViewName("modificaImpiegato");
		return modelAndView;
	}

	@PostMapping("/aggiornaImpiegato/{id}-oldCodiceDip-{oldCodiceDip}")
	public ModelAndView aggiornaImpiegato (@PathVariable("id") Long id, @PathVariable("oldCodiceDip") Long oldCodiceDip, Impiegato impiegato,	BindingResult result, ModelAndView modelAndView) {
		
		impiegatoRepository.save(impiegato);
		//Aggiorno numero di dipendenti di un Dipartimento
		if (oldCodiceDip != null && impiegato.getDipartimento() != null && !impiegato.getDipartimento().getCodiceDip().equals(oldCodiceDip) ){
			Dipartimento oldDipartimento = dipartimentoRepository.findById(oldCodiceDip).orElseThrow(()  -> new IllegalArgumentException("Id Impiegato non valido, id:" + id));
			oldDipartimento.setNumeroImp(oldDipartimento.getNumeroImp() -1);
			dipartimentoRepository.save(oldDipartimento);

			Dipartimento newDipartimento = dipartimentoRepository.findById(impiegato.getDipartimento().getCodiceDip()).orElseThrow(()  -> new IllegalArgumentException("Id Impiegato non valido, id:" + id));
			newDipartimento.setNumeroImp(newDipartimento.getNumeroImp() + 1);
			dipartimentoRepository.save(newDipartimento);
		}
		else if (oldCodiceDip == null  && impiegato.getDipartimento() != null) {
			Dipartimento newDipartimento = dipartimentoRepository.findById(impiegato.getDipartimento().getCodiceDip()).orElseThrow(()  -> new IllegalArgumentException("Id Impiegato non valido, id:" + id));
			newDipartimento.setNumeroImp(newDipartimento.getNumeroImp() + 1);
			dipartimentoRepository.save(newDipartimento);
		}

		else if (oldCodiceDip != null  && impiegato.getDipartimento() == null) {
			Dipartimento oldDipartimento = dipartimentoRepository.findById(oldCodiceDip).orElseThrow(()  -> new IllegalArgumentException("Id Impiegato non valido, id:" + id));
			oldDipartimento.setNumeroImp(oldDipartimento.getNumeroImp() -1);
			dipartimentoRepository.save(oldDipartimento);
		}

		String messaggio = "Impiegato modificato con successo";
		modelAndView = new ModelAndView("redirect:/impiegati?msg=" + messaggio);
		return modelAndView;
	}

	@GetMapping("/eliminaImpiegato/{id}")
	public ModelAndView eliminaImpiegato(@PathVariable("id") Long id, ModelAndView modelAndView) {
		Impiegato impiegato = impiegatoRepository.findById(id).orElseThrow(() -> new ImpiegatoNotFoundException("Invalid Impiegato Id:" + id));
		List<Progetto> responsabileProgetto = progettoRepository.cercaResponsabileProgetto(id);
		
		if(impiegatoRepository.contaSubordinati(id)>1) {  //Controllo per non eliminare un responsabile con subordinati
			String messaggio = "ATTENZIONE! NON PUOI ELIMINARE UN IMPIEGATO CHE E' RESPONSABILE DI ALTRI IMPIEGATI";
			modelAndView = new ModelAndView("redirect:/impiegati?msg=" + messaggio);
			return modelAndView;
		}
		else if (!responsabileProgetto.isEmpty()) {	  	//Controllo per non eliminare un responsabile di un Progetto
			String messaggio = "ATTENZIONE! NON PUOI ELIMINARE UN IMPIEGATO CHE E' RESPONSABILE DI UN PROGETTO";
			modelAndView = new ModelAndView("redirect:/impiegati?msg=" + messaggio);
			return modelAndView;
		} else {
			impiegatoRepository.delete(impiegato);
			Dipartimento dipartimento = impiegato.getDipartimento();
			dipartimento.setNumeroImp(dipartimento.getNumeroImp() - 1);
			dipartimentoRepository.save(dipartimento);
			String messaggio = "Impiegato eliminato con successo";
			modelAndView = new ModelAndView("redirect:/impiegati?msg=" + messaggio);
			return modelAndView;
		}
	}

	@GetMapping("/cercaImpiegato")
	public ModelAndView cercaImpiegato(@RequestParam String keyword) {
		List<Impiegato> risultati = impiegatoRepository.cerca(keyword);
		ModelAndView modelAndView = new ModelAndView("cercaImpiegato");
		modelAndView.addObject("risultati", risultati);
		System.out.println(risultati);
		return modelAndView;    
	}




}