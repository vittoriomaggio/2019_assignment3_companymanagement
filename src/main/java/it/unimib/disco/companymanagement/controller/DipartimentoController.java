package it.unimib.disco.companymanagement.controller;

import it.unimib.disco.companymanagement.exception.DipartimentoNotFoundException;
import it.unimib.disco.companymanagement.model.Dipartimento;
import it.unimib.disco.companymanagement.model.Impiegato;
import it.unimib.disco.companymanagement.repository.DipartimentoRepository;
import it.unimib.disco.companymanagement.repository.ImpiegatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;



@Controller
public class DipartimentoController {

	private final DipartimentoRepository dipartimentoRepository;
	private final ImpiegatoRepository impiegatoRepository;


	@Autowired
	public DipartimentoController(DipartimentoRepository dipartimentoRepository, ImpiegatoRepository impiegatoRepository) {
	  this.impiegatoRepository = impiegatoRepository;
	  this.dipartimentoRepository = dipartimentoRepository;
	  if (dipartimentoRepository.findById(Long.valueOf(1)).isEmpty()) { //Creazione dipartimento base
		Dipartimento  dipartimentoBase = new Dipartimento("DipartimentoBase", 0);
		dipartimentoRepository.save(dipartimentoBase);
	  }
	}

	@GetMapping("/dipartimenti")
	public ModelAndView dipartimenti(@RequestParam(required=false) String msg) {
		ModelAndView modelAndView = new ModelAndView();
		Iterable<Dipartimento> dipartimenti = dipartimentoRepository.findAll();
		modelAndView.addObject("dipartimenti", dipartimenti);
		modelAndView.addObject("messaggio", msg);
		modelAndView.setViewName("dipartimenti");
		return modelAndView;
	}

	@GetMapping("/dipartimenti/aggiungiDipartimento")
	public ModelAndView aggiungiDipartimento(Dipartimento dipartimento, ModelAndView modelAndView, BindingResult bindingResult) {
		modelAndView.addObject("dipartimento", dipartimento);
		modelAndView.setViewName("aggiungiDipartimento");
		return modelAndView;
	}

	@PostMapping("/salvaDipartimento")
	public ModelAndView salvaDipartimento(Dipartimento dipartimento, BindingResult userloginResult, ModelAndView modelAndView) throws DipartimentoNotFoundException {
		dipartimentoRepository.save(dipartimento);
		dipartimentoRepository.findById(dipartimento.getCodiceDip()).orElseThrow(() -> new DipartimentoNotFoundException("Errore durante il salvataggio, riprovare"));
		String messaggio = "Dipartimento salvato con successo!";
		return new ModelAndView("redirect:/dipartimenti?msg=" + messaggio);
	}

	@GetMapping("/dipartimenti/modificaDipartimento/{codiceDip}")
	public ModelAndView modificaDipartimento (@PathVariable("codiceDip") long codiceDip, ModelAndView modelAndView) {

		Dipartimento dipartimento = dipartimentoRepository.findById(codiceDip).orElseThrow(() -> new DipartimentoNotFoundException("Invalid  Id:" + codiceDip));
		modelAndView.addObject("dipartimento", dipartimento);
		modelAndView.setViewName("modificaDipartimento");
		return modelAndView;
	}

	@PostMapping("/aggiornaDipartimento/{codiceDip}")
	public ModelAndView aggiornaDipartimento (@PathVariable("codiceDip") long codiceDip, Dipartimento dipartimento,	BindingResult result, ModelAndView modelAndView) {
		dipartimentoRepository.save(dipartimento);
		String messaggio = "Dipartimento modificato con successo!";
		modelAndView = new ModelAndView("redirect:/dipartimenti?msg="+ messaggio);
		return modelAndView;
	}

	@GetMapping("/eliminaDipartimento/{codiceDip}")
	public ModelAndView eliminaDipartimento(@PathVariable("codiceDip") long codiceDip,  ModelAndView modelAndView) {
		if (codiceDip != 1) { //Non è possibile eliminare il dipartimento base (codiceDip: 1)
			Dipartimento dipartimento = dipartimentoRepository.findById(codiceDip).orElseThrow(() -> new DipartimentoNotFoundException("Invalid Dipartimento Id(CodiceDip):" + codiceDip));
			List<Impiegato> dipartimentoImpiegato = impiegatoRepository.cercaDipartimentoImpiegato(codiceDip);
			if (!dipartimentoImpiegato.isEmpty()) {
				for (Impiegato impiegato : dipartimentoImpiegato) { //Assegnazione dipartimento base
					Dipartimento dipartimentoBase = dipartimentoRepository.findById(Long.valueOf(0)).get();
					impiegato.setDipartimento(dipartimentoBase);
				}
			}
			dipartimentoRepository.delete(dipartimento);
			String messaggio = "Dipartimento eliminato con successo!";
			modelAndView = new ModelAndView("redirect:/dipartimenti?msg=" + messaggio);
			return modelAndView;
		}
		else {
			String messaggio = "Non puoi eliminare il Dipartimento 1, è il dipartimento base!";
			modelAndView = new ModelAndView("redirect:/dipartimenti?msg=" + messaggio);
			return modelAndView;

		}
	}


	@GetMapping("/cercaDipartimento")
	public ModelAndView cercaDipartimento(@RequestParam String keyword) {
		List<Dipartimento> risultati = dipartimentoRepository.cerca(keyword);
		ModelAndView modelAndView = new ModelAndView("cercaDipartimento");
		modelAndView.addObject("risultati", risultati);
		System.out.println(risultati);
		return modelAndView;
	}
}