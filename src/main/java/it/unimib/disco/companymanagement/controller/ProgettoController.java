package it.unimib.disco.companymanagement.controller;

import it.unimib.disco.companymanagement.exception.ProgettoNotFoundException;
import it.unimib.disco.companymanagement.model.Cliente;
import it.unimib.disco.companymanagement.model.Impiegato;
import it.unimib.disco.companymanagement.model.Progetto;
import it.unimib.disco.companymanagement.repository.ClienteRepository;
import it.unimib.disco.companymanagement.repository.ImpiegatoRepository;
import it.unimib.disco.companymanagement.repository.ProgettoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Set;

@Controller
public class ProgettoController {
    private final ProgettoRepository progettoRepository;
    private final ImpiegatoRepository impiegatoRepository;
    private final ClienteRepository clienteRepository;

    @Autowired
    public ProgettoController(ProgettoRepository progettoRepository, ImpiegatoRepository impiegatoRepository, ClienteRepository clienteRepository){
        this.progettoRepository = progettoRepository;
        this.impiegatoRepository = impiegatoRepository;
        this.clienteRepository = clienteRepository;
    }

    @GetMapping("/progetti")
    public ModelAndView progetti(@RequestParam(required=false) String msg) {
        ModelAndView modelAndView = new ModelAndView();
        Iterable<Progetto> progetti = progettoRepository.findAll();
        modelAndView.addObject("progetti", progetti);
        modelAndView.addObject("messaggio", msg);
        modelAndView.setViewName("progetti");
        return modelAndView;
    }

    @GetMapping("/progetti/aggiungiProgetto")
    public ModelAndView aggiungiProgetto(Progetto progetto, ModelAndView modelAndView, BindingResult bindingResult) {
        modelAndView = new ModelAndView();
        progetto = new Progetto();
        Iterable<Impiegato> impiegati = impiegatoRepository.findAll();
        Iterable<Cliente> clienti = clienteRepository.findAll();
        modelAndView.addObject("impiegati", impiegati);
        modelAndView.addObject("clienti", clienti);
        modelAndView.addObject("progetto", progetto);
        modelAndView.setViewName("aggiungiProgetto");
        return modelAndView;
    }

    @PostMapping("/salvaProgetto")
    public ModelAndView salvaProgetto(Progetto progetto, BindingResult userloginResult, ModelAndView modelAndView) {
        modelAndView = new ModelAndView();
        Set<Impiegato> impiegatiCoinvolti = progetto.getImpiegatiCoinvolti();
        impiegatiCoinvolti.add(progetto.getResponsabileProgetto());
        //Aggiornamento lista impiegati coinvolti in progetto
        progetto.setImpiegatiCoinvolti(impiegatiCoinvolti);
        progettoRepository.save(progetto);
        progettoRepository.findById(progetto.getCodiceProgetto()).orElseThrow(() -> new ProgettoNotFoundException("Errore durante il salvataggio, riprovare"));
        String messaggio = "Progetto aggiunto con successo!";
        return new ModelAndView("redirect:/progetti?msg=" + messaggio);
    }

    @GetMapping("/progetti/modificaProgetto/{codiceProgetto}")
    public ModelAndView modificaProgetto (@PathVariable("codiceProgetto") long codiceProgetto, ModelAndView modelAndView) {
        Progetto progetto = progettoRepository.findById(codiceProgetto)
                .orElseThrow(() -> new ProgettoNotFoundException("Codice progetto non valido: " + codiceProgetto));
        Iterable<Impiegato> impiegati = impiegatoRepository.findAll();
        Iterable<Cliente> clienti = clienteRepository.findAll();
        modelAndView.addObject("progetto", progetto);
        modelAndView.addObject("impiegati", impiegati);
        modelAndView.addObject("clienti", clienti);
        modelAndView.setViewName("modificaProgetto");
        return modelAndView;
    }

    @PostMapping("/aggiornaProgetto/{codiceProgetto}")
    public ModelAndView aggiornaProgetto (@PathVariable("codiceProgetto") long codiceProgetto, Progetto progetto, BindingResult result, ModelAndView modelAndView) {
        Set<Impiegato> impiegatiCoinvolti = progetto.getImpiegatiCoinvolti();
        impiegatiCoinvolti.add(progetto.getResponsabileProgetto());
        //Aggiornamento lista impiegati coinvolti in progetto
        progetto.setImpiegatiCoinvolti(impiegatiCoinvolti);
        progettoRepository.save(progetto);
        String messaggio = "Progetto modificato con successo";
        modelAndView = new ModelAndView("redirect:/progetti?msg="+ messaggio);
        return modelAndView;
    }

  
	@GetMapping("/cercaProgetto")
	public ModelAndView cercaProgetto(@RequestParam String keyword) {
		List<Progetto> risultati = progettoRepository.cerca(keyword);
		ModelAndView modelAndView = new ModelAndView("cercaProgetto");
		modelAndView.addObject("risultati", risultati);
		return modelAndView;    
	}

}
