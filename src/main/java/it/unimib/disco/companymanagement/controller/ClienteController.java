package it.unimib.disco.companymanagement.controller;

import it.unimib.disco.companymanagement.exception.ClienteNotFoundException;
import it.unimib.disco.companymanagement.model.Cliente;
import it.unimib.disco.companymanagement.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;



@Controller
public class ClienteController {

	private final ClienteRepository clienteRepository;

	@Autowired
	public ClienteController(ClienteRepository clienteRepository) {
	  this.clienteRepository = clienteRepository;
	}

	@GetMapping("/clienti")
	public ModelAndView clienti(@RequestParam(required=false) String msg) {
		ModelAndView modelAndView = new ModelAndView();
		Iterable<Cliente> clienti = clienteRepository.findAll();

		modelAndView.addObject("clienti", clienti);
		modelAndView.addObject("messaggio", msg);
		modelAndView.setViewName("clienti");

		return modelAndView;
	}

	@GetMapping("clienti/aggiungiCliente")
	public ModelAndView aggiungiCliente(Cliente cliente, ModelAndView modelAndView, BindingResult bindingResult) {
		cliente = new Cliente();

		modelAndView.addObject("clienti", cliente);
		modelAndView.setViewName("aggiungiCliente");
		return modelAndView;
	}

	@PostMapping("/salvaCliente")
	public ModelAndView salvaCliente(Cliente cliente, BindingResult userloginResult, ModelAndView modelAndView) throws ClienteNotFoundException {
		clienteRepository.save(cliente);
		clienteRepository.findById(cliente.getId()).orElseThrow(() -> new ClienteNotFoundException("Errore durante il salvataggio, riprovare"));
		String messaggio = "Cliente salvato con successo!";
		modelAndView = new ModelAndView("redirect:/clienti?msg=" + messaggio);
		return modelAndView;
	}


	@GetMapping("/clienti/modificaCliente/{id}")
	public ModelAndView modificaCLiente(@PathVariable("id") long id, ModelAndView modelAndView) {
		Cliente cliente = clienteRepository.findById(id).orElseThrow(() -> new ClienteNotFoundException("Id cliente non valido, id:" + id));
		modelAndView.addObject("cliente", cliente);
		modelAndView.setViewName("modificaCliente");
		return modelAndView;
	}

	@PostMapping("/aggiornaCliente/{id}")
	public ModelAndView aggiornaCliente(@PathVariable("id") long id, Cliente cliente,	BindingResult result, ModelAndView modelAndView) {

		clienteRepository.save(cliente);
		String messaggio = "Cliente modificato con successo";
		modelAndView = new ModelAndView("redirect:/clienti?msg="+ messaggio);
		return modelAndView;
	}


	@GetMapping("/cercaCliente")
	public ModelAndView cercaCliente(@RequestParam String keyword) {
		List<Cliente> risultati = clienteRepository.cerca(keyword);
		ModelAndView modelAndView = new ModelAndView("cercaCliente");
		modelAndView.addObject("risultati", risultati);
		System.out.println(risultati);
		return modelAndView;    
	}





}