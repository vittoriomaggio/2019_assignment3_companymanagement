package it.unimib.disco.companymanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Nessun Dipartimento trovato")
public class DipartimentoNotFoundException extends RuntimeException {

    public DipartimentoNotFoundException() {
        super();
    }

    public DipartimentoNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public DipartimentoNotFoundException(String message) {
        super(message);
    }

    public DipartimentoNotFoundException(Throwable cause) {
        super(cause);
    }
}


