package it.unimib.disco.companymanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Nessun Progetto trovato")
public class ProgettoNotFoundException extends RuntimeException {

    public ProgettoNotFoundException() {
        super();
    }

    public ProgettoNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProgettoNotFoundException(String message) {
        super(message);
    }

    public ProgettoNotFoundException(Throwable cause) {
        super(cause);
    }
}


