package it.unimib.disco.companymanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Nessun Impiegato trovato")
public class ImpiegatoNotFoundException extends RuntimeException {

    public ImpiegatoNotFoundException() {
        super();
    }

    public ImpiegatoNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ImpiegatoNotFoundException(String message) {
        super(message);
    }

    public ImpiegatoNotFoundException(Throwable cause) {
        super(cause);
    }
}


