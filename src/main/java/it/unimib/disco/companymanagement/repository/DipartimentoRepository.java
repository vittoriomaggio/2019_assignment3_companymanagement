package it.unimib.disco.companymanagement.repository;

import it.unimib.disco.companymanagement.model.Dipartimento;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface DipartimentoRepository extends CrudRepository<Dipartimento, Long> {
    @Query(value = "SELECT i FROM Dipartimento i WHERE "
            + "i.nomeDip LIKE '%' || :keyword || '%'"
            + " OR i.codiceDip LIKE '%' || :keyword || '%'"
            + " OR i.numeroImp LIKE '%' || :keyword || '%'"
    )
    public List<Dipartimento> cerca(@Param("keyword") String keyword);


}
