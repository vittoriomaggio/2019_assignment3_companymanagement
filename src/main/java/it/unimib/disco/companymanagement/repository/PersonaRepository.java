package it.unimib.disco.companymanagement.repository;

import it.unimib.disco.companymanagement.model.Persona;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface PersonaRepository extends CrudRepository<Persona, Long> {}
