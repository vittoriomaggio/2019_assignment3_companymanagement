package it.unimib.disco.companymanagement.repository;

import java.util.List;
import it.unimib.disco.companymanagement.model.Cliente;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository

public interface ClienteRepository extends CrudRepository<Cliente, Long> {

    @Query(value = "SELECT i FROM Cliente i WHERE i.id LIKE '%' || :keyword || '%'"
            + " OR i.email LIKE '%' || :keyword || '%'"
            + " OR i.indirizzo LIKE '%' || :keyword || '%'"
            + " OR i.telefono LIKE '%' || :keyword || '%'"
            + " OR i.denominazioneSociale LIKE '%' || :keyword || '%'"

    )
    public List<Cliente> cerca(@Param("keyword") String keyword);


}