package it.unimib.disco.companymanagement.repository;

import it.unimib.disco.companymanagement.model.Progetto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface ProgettoRepository extends CrudRepository<Progetto, Long> {

    @Query(value = "SELECT p FROM Progetto p WHERE p.codiceProgetto LIKE '%' || :keyword || '%'"
            + " OR p.codiceCliente.id LIKE '%' || :keyword || '%'"
            + " OR p.responsabileProgetto.id LIKE '%' || :keyword || '%'"
            + " OR p.budget LIKE '%' || :keyword || '%'"
            + " OR p.responsabileProgetto.nome LIKE  '%' || :keyword || '%'"
            + " OR p.responsabileProgetto.cognome LIKE '%' || :keyword || '%'"
            + " OR p.codiceCliente.denominazioneSociale LIKE '%' || :keyword || '%'"
            + " OR p.descrizione LIKE '%' || :keyword || '%'"

    )
    List<Progetto> cerca(@Param("keyword") String keyword);

    //Restituisce i Progetti che hanno come responsabile un determinato impiegato
    @Query(value = "SELECT p FROM Progetto p WHERE p.responsabileProgetto.id LIKE :idImpiegato")
    List<Progetto> cercaResponsabileProgetto(@Param("idImpiegato") Long idImpiegato);

    //Restituisce i Progetti che hanno come cliente un determinato cliente
    @Query(value = "SELECT p FROM Progetto p WHERE p.codiceCliente LIKE :codiceCliente")
    List<Progetto> cercaProgettoCliente(@Param("codiceCliente") Long codiceCliente);


}

