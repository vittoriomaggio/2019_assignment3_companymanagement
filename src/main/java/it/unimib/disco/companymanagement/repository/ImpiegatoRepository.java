package it.unimib.disco.companymanagement.repository;

import it.unimib.disco.companymanagement.model.Impiegato;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ImpiegatoRepository extends CrudRepository<Impiegato, Long> {

    @Query(value = "SELECT i FROM Impiegato i WHERE i.id LIKE '%' || :keyword || '%'"
            + " OR i.email LIKE '%' || :keyword || '%'"
            + " OR i.indirizzo LIKE '%' || :keyword || '%'"
            + " OR i.telefono LIKE '%' || :keyword || '%'"
            + " OR i.cognome LIKE '%' || :keyword || '%'"
            + " OR i.nome LIKE '%' || :keyword || '%'"
            + " OR i.dipartimento.codiceDip LIKE '%' || :keyword || '%'"
            + " OR i.responsabile.id LIKE '%' || :keyword || '%'"
            )
    public List<Impiegato> cerca(@Param("keyword") String keyword);

    // Restituisce tutti gli impiegati in base al dipartimento
    @Query(value = "SELECT i FROM Impiegato i WHERE i.dipartimento.codiceDip LIKE :codiceDip")
    public List<Impiegato> cercaDipartimentoImpiegato(@Param("codiceDip") Long codiceDip);

    // Restituisce il numero di Impiegati subordinati ad un responsabile
    @Query(value = "SELECT COUNT(i) FROM Impiegato i WHERE i.responsabile.id LIKE :idResponsabile")
    public int contaSubordinati(@Param("idResponsabile") Long idResponsabile);

}

