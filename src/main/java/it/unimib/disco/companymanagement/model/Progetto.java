package it.unimib.disco.companymanagement.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "PROGETTO")
public class Progetto {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="codiceprog")
    @SequenceGenerator(name = "codiceprog", sequenceName = "codiceprog", initialValue = 1, allocationSize=1)
    @Column(name = "CODICEPROG")
    private Long codiceProgetto;

    @OneToOne //Relazione Progetto - Impiegato
    @JoinColumn(name = "ID_IMPIEGATO")
    private Impiegato responsabileProgetto;

    @OneToOne //Relazione Progetto - Cliente
    @JoinColumn(name = "ID_CLIENTE")
    private Cliente codiceCliente;

    @ManyToMany //Relazione Progetto - Impiegato
    private Set<Impiegato> impiegatiCoinvolti;

    @Column(name = "BUDGET")
    private int budget;

    @Column(name = "DESCRIZIONE")
    private String descrizione;

    public Progetto() {}

    public Progetto(Impiegato responsabileProgetto, Cliente codiceCliente, int budget){
        this.responsabileProgetto = responsabileProgetto;
        this.codiceCliente = codiceCliente;
        this.budget = budget;
    }

    @Override
    public String toString() {
        return String.format("Progetto[codiceProgetto=%d, codiceCliente='%s', responsabile='%d' ,budget='%d']",
                codiceProgetto, codiceCliente, responsabileProgetto, budget);
    }

    //SET
    public void setCodiceProgetto(Long codiceProgetto){
        this.codiceProgetto = codiceProgetto;
    }

    public void setResponsabileProgetto(Impiegato responsabileProgetto){
        this.responsabileProgetto = responsabileProgetto;
    }

    public void setCodiceCliente(Cliente codiceCliente){
        this.codiceCliente = codiceCliente;
    }

    public void setBudget(int budget){
        this.budget = budget;
    }

    public void setImpiegatiCoinvolti(Set<Impiegato> impiegatiCoinvolti){
        this.impiegatiCoinvolti  = impiegatiCoinvolti;
    }

    public void setDescrizione(String descrizione){
        this.descrizione = descrizione;
    }

    //GET
    public Long getCodiceProgetto() {
        return codiceProgetto;
    }

    public Impiegato getResponsabileProgetto() {
        return responsabileProgetto;
    }

    public Cliente getCodiceCliente() {
        return codiceCliente;
    }

    public int getBudget() {
        return budget;
    }

    public Set<Impiegato> getImpiegatiCoinvolti(){
        return impiegatiCoinvolti;
    }

    public String getDescrizione(){
        return descrizione;
    }

    
}
