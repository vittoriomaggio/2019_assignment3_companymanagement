package it.unimib.disco.companymanagement.model;
import javax.persistence.*;

import java.util.Collection;

@Entity
@Table(name = "DIPARTIMENTO")
public class Dipartimento {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="codiceDip")
    @SequenceGenerator(name = "codiceDip", sequenceName = "codiceDip", initialValue = 1, allocationSize=1)
    @Column(name = "CODICEDIP") 
    private Long codiceDip;

    @Column(name = "NOMEDIP")
    private String nomeDip;

    @Column(name = "NUMEROIMP")
    private int numeroImp = 0;

    @OneToMany(mappedBy = "dipartimento")  //Relazione Dipartimento - Impiegati appartengono al dipartimento
    private Collection<Impiegato> imp;

    public Dipartimento() {
    }

    public Dipartimento(String nomeDip, int numeroImp){
        this.nomeDip = nomeDip;
        this.numeroImp = numeroImp;
    }

    @Override
    public String toString() {
        return String.format("Dipartimento[codiceDip='%s', nomeDip='%s', numeroImp='%s']", codiceDip, nomeDip, numeroImp);
    }

    //SET
    public void setCodiceDip(Long codiceDip){
        this.codiceDip = codiceDip;
    }

    public void setNomeDip (String nomeDip){
        this.nomeDip = nomeDip;
    }

    public void setNumeroImp (int numeroImp){
        this.numeroImp = numeroImp;
    }
    //GET
    public Long getCodiceDip() {
        return codiceDip;
    }

    public String getNomeDip() {
        return nomeDip;
    }

    public int getNumeroImp() {
        return numeroImp;
    }
}
