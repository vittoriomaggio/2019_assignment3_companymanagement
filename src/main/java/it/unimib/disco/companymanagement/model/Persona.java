package it.unimib.disco.companymanagement.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
//import javax.persistence.JoinColumn;

import javax.persistence.*;

@Entity
@Table(name = "PERSONA")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Persona {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="persona_id")
  @SequenceGenerator(name = "persona_id", sequenceName = "persona_id", initialValue = 1, allocationSize=1)
  @Column(name = "ID")
  private Long id;

  @Column(name = "EMAIL")
  private String email;

  @Column(name = "TELEFONO", columnDefinition = "BIGINT")
  private long telefono;

  @Column(name = "INDIRIZZO")
  private String indirizzo;

  //Id classi figlie
  @OneToOne 
  private Impiegato idImpiegato;

  @OneToOne
  private Cliente idCliente;

  public Persona() {
  }

  public Persona(Long id, String email, long telefono, String indirizzo) {
    this.id = id;
    this.email = email;
    this.telefono = telefono;
    this.indirizzo = indirizzo;
  }

  @Override
  public String toString() {
    return "Persona{" +
            "codiceID=" + id +
            ", email='" + email + '\'' +
            ", telefono=" + telefono +
            ", indirizzo='" + indirizzo + '\'' +
            '}';
  }

  //SET
  public void setEmail(String email) {
    this.email = email;
  }

  public void setTelefono (long telefono) {
    this.telefono = telefono;
  }

  public void setIndirizzo (String indirizzo) {
    this.indirizzo = indirizzo;
  }

  public void setId (Long id) {
    this.id = id;
  }


  //GET
  public Long getId() {
    return id;
  }

  public String getEmail() {
    return email;
  }

  public long getTelefono() {    
    return telefono; 
  }

  public String getIndirizzo() {
    return indirizzo;
  }

}
