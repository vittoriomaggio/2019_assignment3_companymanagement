package it.unimib.disco.companymanagement.model;

import javax.persistence.*;



import java.util.Collection;

@Entity
@Table(name = "IMPIEGATO")
@PrimaryKeyJoinColumn(name = "MATRICOLA")
public class Impiegato extends Persona {

    @Column(name = "NOME")
    private String nome;

    @Column(name = "COGNOME")
    private String cognome;

    // Relazione Impiegato - Dipartimento
    @ManyToOne
    private Dipartimento dipartimento;

    // Relazione Impiegato - Responsabile (Impiegato)
    @ManyToOne
    private Impiegato responsabile;

    @OneToMany(mappedBy = "responsabile")
    private Collection<Impiegato> subordinato;

    //Relazione Impiegato - Progetto 
    @ManyToMany(mappedBy="impiegatiCoinvolti")
    private Collection<Progetto> progetti;
 
    public Impiegato(){}

    public Impiegato(long id, String email, long telefono, String indirizzo, String nome, String cognome, Dipartimento dipartimento, Impiegato responsabile){
        super(id, email, telefono, indirizzo);
        this.nome = nome;
        this.cognome = cognome;
        this.dipartimento = dipartimento;
        this.responsabile = responsabile;
    }

    // SET
    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    

    public void setDipartimento(Dipartimento dipartimento) {
        this.dipartimento = dipartimento;
    }

    public void setResponsabile(Impiegato responsabile) {
        this.responsabile = responsabile;
    }

    public void setProgetti(Collection<Progetto> progetti) {
        this.progetti = progetti;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public String getNome() {
        return nome;
    }

    public String getCognome() {
        return cognome;
    }


    public Dipartimento getDipartimento() {
        return dipartimento;
    }

    public Impiegato getResponsabile() {
        return responsabile;
    }

    public Collection<Progetto> getProgetti(){
        return this.progetti;
    }

}
