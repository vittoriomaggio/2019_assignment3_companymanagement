package it.unimib.disco.companymanagement.model;
import javax.persistence.*;

@Entity
@Table(name = "CLIENTE")
public class Cliente extends Persona{
  

  @Column(name = "DENOMINAZIONESOCIALE")
  private String denominazioneSociale;
  

  public Cliente() {
  }

  public Cliente(long id, String denominazioneSociale, String email, long telefono, String indirizzo) {
    super(id, email, telefono, indirizzo);
    this.denominazioneSociale=denominazioneSociale;
  }


  @Override
  public String toString() {
    return String.format("Cliente[pIva='%s', denominazioneSociale='%s']", this.getId(), this.getDenominazioneSociale());
  }

  //SET
  public void setDenominazioneSociale(String denominazioneSociale) {
    this.denominazioneSociale = denominazioneSociale;
  }

  //GET
  public String getDenominazioneSociale() {
    return denominazioneSociale;
  }

  
}
